<?php
require_once __DIR__.'/../vendor/autoload.php';

use mef\Validation\Sanitizer\RegularExpressionSanitizer;
use mef\Validation\Sanitizer\TemplateSanitizer;

/*
use mef\Stdlib\ArrayUtil;

$input = [
	'array' => ['',''],
	'int' => 0,
	'boolean' => false,
	'float' => 0.0,
	'empty_string' => '',
	'zero_string' => '0'
];

// mef\Stdlib\ArrayUtil::sanitize($input, new mef\Validation\Null(), true);
# var_dump($input);
*/

$template = [
	'person[]' => [
		'name' => 'string',
		'gender' => ['male', 'female'],
		'iq' => 'integer',
		'is_admin' => 'boolean!',
		'score' => 'float',
		'birth_date' => 'datetime',
		'children[]' => 'string'
	],

	'action' => new RegularExpressionSanitizer('/foo/', 'bar'),
/*
	'zend[]' => new mef\Validation\SanitizationChain([
		new mef\Validation\Zend(new Zend\Filter\Null()),
		new mef\Validation\DefaultValue('yes, man')
	])
*/
];

$inputFilter = new TemplateSanitizer($template);

$input = [
	'person' => [
		[ 'name' => 'James', 'birth_date' => '', 'gender' => 'male', 'iq' => 165, 'children' => ['Jimmy'] ],
		[ 'name' => 'Henry', 'is_admin' => 1, 'score' => 0.36, 'children' => ['Sarah', 'Jeff'] ],
		[ 'name' => ' Sam ', 'is_admin' => 2, 'birth_date' => '10/22/1986', 'score' => '' ],
		42
	],

	'action' => 'foobar',

	'zend' => ['0', 42 => 'bar', 'zed' => '0']
];

$results = $inputFilter->sanitizeWithResults($input);

$data = $results['output'];

var_dump($data);
