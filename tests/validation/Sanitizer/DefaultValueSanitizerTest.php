<?php namespace mef\UnitTest\Validation;

use mef\Validation\Sanitizer\DefaultValueSanitizer;

require_once __DIR__ . '/../../MefworksUnitTest.php';

/**
 * @coversDefaultClass \mef\Validation\Sanitizer\DefaultValueSanitizer
 */
class DefaultValueSanitizerTest extends \MefworksTestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getDefaultValue
	 */
	public function testConstructor()
	{
		$defaultValueSanitizer = new DefaultValueSanitizer(42);
		$this->assertSame(42, $defaultValueSanitizer->getDefaultValue());
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitize()
	{
		$defaultValueSanitizer = new DefaultValueSanitizer(42);
		$this->assertSame(42, $defaultValueSanitizer->sanitize(null));
		$this->assertSame(42, $defaultValueSanitizer->sanitize(''));
		$this->assertSame(0, $defaultValueSanitizer->sanitize(0));
	}
}