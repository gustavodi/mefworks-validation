<?php namespace mef\UnitTest\Validation;

use mef\Validation\Sanitizer\ZendSanitizer;
use Zend\Filter\FilterInterface;

require_once __DIR__ . '/../../MefworksUnitTest.php';

/**
 * @coversDefaultClass \mef\Validation\Sanitizer\ZendSanitizer
 */
class ZendSanitizerTest extends \MefworksTestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getZendFilter
	 */
	public function testConstructor()
	{
		$zendFilter = $this->getMockBuilder(FilterInterface::class)->getMock();

		$zendSanitizer = new ZendSanitizer($zendFilter);
		$this->assertSame($zendFilter, $zendSanitizer->getZendFilter());
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitize()
	{
		$zendFilter = $this->getMockBuilder(FilterInterface::class)->setMethods(['filter'])->getMock();
		$zendFilter->expects($this->once())->method('filter')->willReturn(42);

		$zendSanitizer = new ZendSanitizer($zendFilter);
		$this->assertSame(42, $zendSanitizer->sanitize('test'));
	}
}