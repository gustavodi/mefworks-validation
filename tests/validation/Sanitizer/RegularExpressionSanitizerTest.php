<?php namespace mef\UnitTest\Validation;

use mef\Validation\Sanitizer\RegularExpressionSanitizer;

require_once __DIR__ . '/../../MefworksUnitTest.php';

/**
 * @coversDefaultClass \mef\Validation\Sanitizer\RegularExpressionSanitizer
 */
class RegularExpressionSanitizerTest extends \MefworksTestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getExpression
	 * @covers ::getReplacement
	 */
	public function testConstructor()
	{
		$regexpSanitizer = new RegularExpressionSanitizer('/foo/', 'bar');
		$this->assertSame('/foo/', $regexpSanitizer->getExpression());
		$this->assertSame('bar', $regexpSanitizer->getReplacement());
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitize()
	{
		$regexpSanitizer = new RegularExpressionSanitizer('/foo/');

		$this->assertSame('foofoo', $regexpSanitizer->sanitize('foofoo'));
	}

	/**
	 * @covers ::sanitize
	 */
	public function testInvalidSanitize()
	{
		$this->expectException(\mef\Validation\Exception\IllegalCastException::class);
		$regexpSanitizer = new RegularExpressionSanitizer('/foo/');

		$regexpSanitizer->sanitize('bar');
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeWithReplacement()
	{
		$regexpSanitizer = new RegularExpressionSanitizer('/foo/', 'bar');

		$this->assertSame('test', $regexpSanitizer->sanitize('test'));
		$this->assertSame('testbarbar', $regexpSanitizer->sanitize('testfoofoo'));
	}
}