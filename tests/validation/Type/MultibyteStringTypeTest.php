<?php namespace mef\UnitTest\Validation;

use mef\Validation\Type\MultibyteStringType;

require_once __DIR__ . '/../../MefworksUnitTest.php';

class TestStringClass
{
	public function __toString()
	{
		return '42';
	}
}

/**
 * @coversDefaultClass \mef\Validation\Type\MultibyteStringType
 */
class MultibyteStringTypeTest extends \MefworksTestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getEncoding
	 */
	public function testGetEncoding()
	{
		$stringType = new MultibyteStringType('UTF-16');
		$this->assertSame('UTF-16', $stringType->getEncoding());
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeValidString()
	{
		$stringType = new MultibyteStringType;

		$string = chr(194) . chr(161) . 'Hello, World!';
		$this->assertSame($string, $stringType->sanitize($string));
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeInteger()
	{
		$stringType = new MultibyteStringType;
		$this->assertSame('42', $stringType->sanitize(42));
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeStringObject()
	{
		$stringType = new MultibyteStringType;
		$this->assertSame('42', $stringType->sanitize(new TestStringClass));
	}

	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeInvalidString()
	{
		$this->expectException(\mef\Validation\Exception\IllegalCastException::class);
		$stringType = new MultibyteStringType;
		$stringType->sanitize(chr(200) . 'Hello, World!');
	}

	/**
	 * @covers ::sanitize
	 */
	public function testInvalidObject()
	{
		$this->expectException(\mef\Validation\Exception\IllegalCastException::class);
		$stringType = new MultibyteStringType;
		$stringType->sanitize($stringType);
	}

	/**
	 * @covers ::validate
	 */
	public function testValidate()
	{
		$stringType = new MultibyteStringType;

		$this->assertTrue($stringType->validate(chr(194) . chr(161) . 'Hello, World!'));
		$this->assertFalse($stringType->validate(chr(200) . 'Hello, World!'));
	}
}