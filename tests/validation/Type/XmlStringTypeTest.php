<?php namespace mef\UnitTest\Validation;

use mef\Validation\Type\XmlStringType;

require_once __DIR__ . '/../../MefworksUnitTest.php';

/**
 * @coversDefaultClass \mef\Validation\Type\XmlStringType
 */
class XmlStringTypeTest extends \MefworksTestCase
{
	/**
	 * @covers ::sanitize
	 */
	public function testSanitizeValidString()
	{
		$stringType = new XmlStringType;

		$string = 'Valid String';
		$this->assertSame($string, $stringType->sanitize($string));
	}

	/**
	 * @covers ::validate
	 */
	public function testValidate()
	{
		$stringType = new XmlStringType;

		$this->assertTrue($stringType->validate('Hello, World!'));
		$this->assertFalse($stringType->validate('test' . chr(250) . 'Hello, World!'));
		$this->assertFalse($stringType->validate('test' . chr(0) . 'Hello, World!'));
	}
}