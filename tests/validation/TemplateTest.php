<?php namespace mef\UnitTest\Validation;

use mef\Validation\Type\BooleanType;
use mef\Validation\Type\DateTimeType;
use mef\Validation\Type\FloatType;
use mef\Validation\Type\IntegerType;
use mef\Validation\Type\MultibyteStringType;

require_once __DIR__ . '/../MefworksUnitTest.php';

class TemplateTest extends \MefworksTestCase
{
	public function testTypeMap()
	{
		$typeMap = new \mef\Validation\Sanitizer\TypeMap();

		$this->assertTrue($typeMap->getSanitizer('boolean') instanceof BooleanType);
		$this->assertTrue($typeMap->getSanitizer('datetime') instanceof DateTimeType);
		$this->assertTrue($typeMap->getSanitizer('float') instanceof FloatType);
		$this->assertTrue($typeMap->getSanitizer('integer') instanceof IntegerType);
		$this->assertTrue($typeMap->getSanitizer('string') instanceof MultibyteStringType);

		try
		{
			$typeMap->getSanitizer('bogus');
			$this->assertTrue(false);
		}
		catch (\mef\Validation\Exception\InvalidArgumentException $e)
		{
			$this->assertTrue(true);
		}
	}

	public function testTemplate()
	{
		$input = [
			'planets' => [
				['name' => 'Mercury', 'type' => 'terrestrial', 'diameter' => 0.382],
				['name' => 'Venus', 'type' => 'terrestrial', 'diameter' => 0.949],
				'earth' => ['name' => 'Earth', 'habitable' => true, 'moons' => 1, 'diameter' => 1.0],
				['name' => 'Mars', 'type' => 'terrestrial', 'moons' => 2, 'diameter' => 0.532],
				['name' => 'Jupiter', 'type' => 'gas', 'moons' => 67, 'diameter' => 11.209],
				['name' => 'Saturn', 'type' => 'gas', 'moons' => 62, 'diameter' => 9.449],
				['name' => 'Uranus', 'type' => 'gas', 'moons' => 27, 'diameter' => 4.007],
				['name' => 'Neptune', 'type' => 'gas', 'moons' => 13, 'diameter' => 3.883],
				['name' => 'Pluto', 'type' => 'dwarf', 'moons' => 5, 'diameter' => 0.16]
			],

			'animals' => [
				'lion' => ['class' => 'mammal', 'sound' => 'roar', 'legs' => 4],
				'dove' => ['class' => 'bird', 'sound' => 'coo', 'legs' => 2],
				'salmon' => ['class' => 'fish']
			]
		];

		$template = [
			'planets[]' => [
				'name' => 'string',
				'type' => ['terrestrial', 'gas', 'dwarf'],
				'habitable' => 'boolean!',
				'moons' => new IntegerType,
				'diameter' => 'float!',
			],

			'animals[string]' => [
				'class' => ['amphibian', 'bird', 'fish', 'mammal', 'reptile'],
				'sound' => function ($value) { return (string) $value; },
				'legs' => 'integer!'
			]

		];

		$sanitizer = new \mef\Validation\Sanitizer\TemplateSanitizer($template);

		$this->assertTrue($sanitizer->getTypeMap() instanceof \mef\Validation\Sanitizer\TypeMap);

		$output = $sanitizer->sanitize($input);
		$this->assertEquals(0, $output['planets'][0]['moons']);
		$this->assertEquals('Earth', $output['planets'][2]['name']);
		$this->assertFalse($output['planets'][8]['habitable']);
		$this->assertEquals('dwarf', $output['planets'][8]['type']);
		$this->assertEquals('bird', $output['animals']['dove']['class']);
	}

	public function testDuplicateKey()
	{
		$this->expectException(\mef\Validation\Exception\DuplicateTemplateKeyException::class);
		$sanitizer = new \mef\Validation\Sanitizer\TemplateSanitizer([
			'foo[]' => 'string',
			'foo' => 'string'
		]);

		$sanitizer->sanitize([]);
	}

	public function testEmptyKey()
	{
		$this->expectException(\mef\Validation\Exception\InvalidTemplateKeyException::class);
		$sanitizer = new \mef\Validation\Sanitizer\TemplateSanitizer([
			'' => 'string'
		]);

		$sanitizer->sanitize([]);
	}

	public function testEmptyInvalidKeyType()
	{
		$this->expectException(\mef\Validation\Exception\InvalidTemplateTypeException::class);
		$sanitizer = new \mef\Validation\Sanitizer\TemplateSanitizer([
			'foo' => new \stdClass
		]);

		$sanitizer->sanitize([]);
	}
}