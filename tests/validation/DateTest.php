<?php namespace mef\UnitTest\Validation;

use DateTime;
use DateTimeImmutable;
use mef\Validation\Type\DateTimeType;

require_once __DIR__ . '/../MefworksUnitTest.php';

class DateTest extends \MefworksTestCase
{
	public function testDateTime()
	{
		$datetime = new DateTimeType;
		$this->assertNull($datetime->getInputFormat());

		// valid date format
		$this->assertTrue($datetime->sanitize('2013-01-30') instanceof DateTimeImmutable);
		$this->assertTrue($datetime->sanitize(new DateTime) instanceof DateTimeImmutable);
		$this->assertTrue($datetime->sanitize(new DateTimeImmutable) instanceof DateTimeImmutable);

		$datetime = new DateTimeType('Y');

		$this->assertEquals('2013', $datetime->sanitize('2013')->format('Y'));

		$this->assertTrue($datetime->validate(new DateTimeImmutable));
		$this->assertFalse($datetime->validate('2013-01-01'));
	}

	public function testInvalidDate()
	{
		$this->expectException(\mef\Validation\Exception\IllegalCastException::class);
		$datetime = new DateTimeType;
		$datetime->sanitize('2013-30-01');
	}

	public function testMutableDate()
	{
		$dateType = new DateTimeType;
		$dateTime = new DateTime;

		$this->assertTrue($dateType->sanitize($dateTime) instanceof DateTimeImmutable);
	}

	public function testInvalidType()
	{
		$this->expectException(\mef\Validation\Exception\IllegalCastException::class);
		$datetime = new DateTimeType;
		$this->assertNull($datetime->sanitize(1));
	}
}