<?php namespace mef\Validation\Sanitizer;

class TrimSanitizer implements SanitizationInterface
{
	private $charlist = '';

	public function __construct($charlist = " \t\n\r\0\x0B")
	{
		$this->charlist = $charlist;
	}

	public function sanitize($value)
	{
		return trim($value, $this->charlist) ?: '';
	}
}