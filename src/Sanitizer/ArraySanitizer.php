<?php namespace mef\Validation\Sanitizer;

use InvalidArgumentException;
use Traversable;
use mef\Validation\Exception\IllegalCastException;

class ArraySanitizer implements SanitizationInterface
{
	private $valueSanitizer;
	private $keySanitizer;

	public function __construct(SanitizationInterface $valueSanitizer, SanitizationInterface $keySanitizer = null)
	{
		$this->valueSanitizer = $valueSanitizer;
		$this->keySanitizer = $keySanitizer;
	}

	public function sanitize($values)
	{
		return $this->sanitizeWithResults($values)['output'];
	}

	public function sanitizeWithResults($values)
	{
		$output = [];
		$errors = [];

		if (!is_array($values) && !($values instanceof Traversable))
		{
			throw new IllegalCastException;
		}

		$index = 0;

		foreach ($values as $inputKey => $value)
		{
			try
			{
				$value = $this->valueSanitizer->sanitize($value);
			}
			catch (IllegalCastException $e)
			{
				$errors[$inputKey]['value'] = $e;
			}

			if ($this->keySanitizer !== null)
			{
				try
				{
					$key = $this->keySanitizer->sanitize($inputKey);
				}
				catch (IllegalCastException $e)
				{
					$errors[$inputKey]['key'] = $e;
				}
			}
			else
			{
				$key = $index++;
			}

			if (isset($errors[$inputKey]) === false)
			{
				$output[$key] = $value;
			}
		}

		return [
			'input' => $values,
			'output' => $output,
			'errors' => $errors,
		];
	}
}