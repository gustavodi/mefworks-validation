<?php namespace mef\Validation\Sanitizer;

interface TemplateTypeMapInterface
{
	public function getSanitizer($type);
}
