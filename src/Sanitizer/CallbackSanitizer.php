<?php namespace mef\Validation\Sanitizer;

class CallbackSanitizer implements SanitizationInterface
{
	private $callback;

	public function __construct(callable $callback)
	{
		$this->callback = $callback;
	}

	public function sanitize($value)
	{
		$callback = $this->callback;
		return $callback($value);
	}
}