<?php namespace mef\Validation\Sanitizer;

use mef\Validation\Exception\InvalidArgumentException;

/**
 * An immutable set of sanitization filters to run in-order.
 */

class ChainSanitizer implements SanitizationInterface, \IteratorAggregate
{
	private $sanitizers = [];

	/**
	 * Constructor
	 *
	 * @param array<SanitizationInterface> $sanitizers
	 */
	public function __construct(array $sanitizers = [])
	{
		foreach ($sanitizers as $sanitizer)
		{
			if (!$sanitizer instanceof SanitizationInterface)
			{
				throw new InvalidArgumentException('Every element must be a SanitizationInterface');
			}
		}

		$this->sanitizers = $sanitizers;
	}

	/**
	 * Return an iterator that iterates over each sanitizer
	 *
	 * @return Iterator
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->sanitizers);
	}

	/**
	 * Return an array of sanitizers
	 *
	 * @return array
	 */
	public function getSanitizers()
	{
		return $this->sanitizers;
	}

	/**
	 * Pass the value through each of the sanitizers.
	 *
	 * @param mixed $value   The value to sanitize
	 *
	 * @return mixed  The sanitized value
	 */
	public function sanitize($value)
	{
		foreach ($this->sanitizers as $sanitizer)
		{
			$value = $sanitizer->sanitize($value);
		}

		return $value;
	}
}
