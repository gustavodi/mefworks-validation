<?php namespace mef\Validation\Sanitizer;

use mef\Validation\Type;
use mef\Validation\Exception\InvalidArgumentException;

class TypeMap implements TemplateTypeMapInterface
{
	public function getSanitizer($type)
	{
		$filters = [
			'float!' => function() {
				return new ChainSanitizer([
					new DefaultValueSanitizer(0.0),
					new Type\FloatType
				]);
			},

			'integer!' => function() {
				return new ChainSanitizer([
					new DefaultValueSanitizer(0),
					new Type\IntegerType
				]);
			},

			'boolean!' => function() {
				return new ChainSanitizer([
					new DefaultValueSanitizer(false),
					new Type\BooleanType
				]);
			},

			'string!' => function() {
				return new ChainSanitizer([
					new DefaultValueSanitizer(''),
					new Type\MultibyteStringType
				]);
			},

			'string' => function() {
				return new Type\MultibyteStringType;
			},

			'integer' => function() {
				return new Type\IntegerType;
			},

			'boolean' => function() {
				return new Type\BooleanType;
			},

			'float' => function() {
				return new Type\FloatType;
			},

			'datetime' => function() {
				return new Type\DateTimeType;
			},
		];

		if (isset($filters[$type]) === false)
		{
			throw new InvalidArgumentException("Unknown type $type");
		}

		if ($filters[$type] instanceof SanitizationInterface === false)
		{
			$filters[$type] = $filters[$type]();
		}

		return $filters[$type];
	}
}
