<?php namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class EnumerationType implements TypeInterface
{
	private $values;

	public function __construct(array $values = [])
	{
		$this->values = array_flip($values);
	}

	public function getValues()
	{
		return array_keys($this->values);
	}

	public function sanitize($value)
	{
		if (isset($this->values[$value]) === false)
		{
			throw new IllegalCastException;
		}

		return $value;
	}

	public function validate($value)
	{
		return isset($this->values[$value]);
	}
}