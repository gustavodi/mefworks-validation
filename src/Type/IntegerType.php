<?php namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class IntegerType implements TypeInterface
{
	/**
	 * Casts the value to an integer.
	 *
	 * @param mixed $value   The value to cast to an integer
	 *
	 * @return int    The integer representation of $value
	 */
	public function sanitize($value)
	{
		if (is_int($value) === true)
		{
			return $value;
		}
		else if (is_string($value) === true)
		{
			if (preg_match('/^-?(0|[1-9][0-9]*)$/', $value))
			{
				return (int) $value;
			}
		}
		else if ($value === true)
		{
			return 1;
		}
		else if ($value === false)
		{
			return 0;
		}

		throw new IllegalCastException;
	}

	/**
	 * Validates that the value is an integer type.
	 *
	 * @param mixed $value   The value to test
	 *
	 * @return bool  true if $value is an integer
	 */
	public function validate($value)
	{
		return is_int($value);
	}
}