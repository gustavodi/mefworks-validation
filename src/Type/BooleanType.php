<?php namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class BooleanType implements TypeInterface
{
	/**
	 * Casts the value to a boolean.
	 *
	 * @param mixed $value   The value to cast to an boolean
	 *
	 * @return bool   The boolean representation of $value
	 */
	public function sanitize($value)
	{
		if ($value === true || $value === '1' || $value === 1)
		{
			return true;
		}
		else if ($value === false || $value === '0' || $value === 0)
		{
			return false;
		}

		throw new IllegalCastException;
	}

	/**
	 * Validates that the value is a boolean type.
	 *
	 * @param mixed $value   The value to test
	 *
	 * @return bool  true if $value is a boolean
	 */
	public function validate($value)
	{
		return is_bool($value);
	}
}