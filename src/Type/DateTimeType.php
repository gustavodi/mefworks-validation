<?php namespace mef\Validation\Type;

use DateTimeInterface;
use DateTimeImmutable;
use mef\Validation\Exception\IllegalCastException;

class DateTimeType implements TypeInterface
{
	private $inputFormat;

	/**
	 * Constructor
	 *
	 * @param string|null $inputFormat  The date format of the data
	 */
	public function __construct($inputFormat = null)
	{
		$this->inputFormat = $inputFormat !== null ? (string) $inputFormat : null;
	}

	/**
	 * @return string
	 */
	public function getInputFormat()
	{
		return $this->inputFormat;
	}

	/**
	 * Return a DateTimeImmutable object from the value.
	 *
	 * @param  string $value
	 * @return \DateTimeImmutable
	 */
	public function sanitize($value)
	{
		if (is_string($value) === true && $value !== '')
		{
			if ($this->inputFormat === null)
			{
				$value = date_create_immutable($value);
			}
			else
			{
				$value = date_create_immutable_from_format($this->inputFormat, $value);
			}

			if ($value === false)
			{
				throw new IllegalCastException;
			}
		}
		else if ($value instanceof DateTimeInterface)
		{
			if (!($value instanceof DateTimeImmutable))
			{
				$value = date_create_immutable($value->format('c'));
			}
		}
		else
		{
			throw new IllegalCastException;
		}

		return $value;
	}

	public function validate($value)
	{
		return $value instanceof DateTimeImmutable;
	}
}
