<?php namespace mef\Validation\Type;

use mef\Validation\Sanitizer\SanitizationInterface;

interface TypeInterface extends SanitizationInterface
{
	public function validate($value);
}