<?php namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

class MultibyteStringType implements TypeInterface
{
	private $encoding;

	public function __construct($encoding = 'UTF-8')
	{
		$this->encoding = (string) $encoding;
	}

	public function getEncoding()
	{
		return $this->encoding;
	}

	public function sanitize($value)
	{
		if (is_string($value))
		{
			if (mb_check_encoding($value, $this->encoding) === false)
			{
				throw new IllegalCastException;
			}

			return $value;
		}
		else if (is_scalar($value) === true)
		{
			return (string) $value;
		}
		else if (is_object($value) === true && method_exists($value, '__toString') === true)
		{
			return $this->sanitize((string) $value);
		}

		throw new IllegalCastException;
	}

	public function validate($value)
	{
		return is_string($value) === true && mb_check_encoding($value, $this->encoding) === true;
	}
}