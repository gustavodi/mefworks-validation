<?php namespace mef\Validation\Type;

use mef\Validation\Exception\IllegalCastException;

/**
 * A sanitization filter to convert data to null if it matches the criteria
 * as configured when creating the \mef\Filter\Null instance.
 *
 * By default nothing (except null) is cast to null.
 *
 * <source>
 * $nullType = new NullType(NullType::EMPTY_STRING);
 * var_dump($nullType->sanitize(''));     // output: null
 * </source>
 *
 * @property-read int $nullSources   A bitmask representing the source values
 *    to be considered null. Possible values:
 *
 *    mef\Validation\Type\NullType::NULL            null
 *    mef\Validation\Type\NullType::EMPTY_STRING    ''
 *    mef\Validation\Type\NullType::EMPTY_ARRAY     []
 *    mef\Validation\Type\NullType::INTEGER         0
 *    mef\Validation\Type\NullType::FLOAT           0.0
 *    mef\Validation\Type\NullType::BOOLEAN         false
 *    mef\Validation\Type\NullType::ZERO_STRING     '0'
 *    mef\Validation\Type\NullType::ALL             all of the above
 */
class NullType implements TypeInterface
{
	const NULL = 0;
	const EMPTY_STRING  = 1;
	const EMPTY_ARRAY = 2;
	const INTEGER = 4;
	const FLOAT = 8;
	const BOOLEAN = 16;
	const ZERO_STRING = 32;
	const ALL = 63;

	private $nullSources;

	/**
	 * Constructor
	 *
	 * @param int $nullSources  a bitmask of which input values should be
	 *                          considered null.
	 *
	 */
	public function __construct($nullSources = self::NULL)
	{
		$this->nullSources = (int) $nullSources;
	}

	/**
	 * If the value is nullable, return null.
	 *
	 * @param mixed $value   The source value to check if it is null
	 *
	 * @return null   null if the value matches one of the sources as
	 *                 specified in the constructor. Else returns original
	 *                 value.
	 */
	public function sanitize($value)
	{
		$s = $this->nullSources;

		if (
			$value === null ||
			(($s & self::EMPTY_STRING) && $value === '') ||
			(($s & self::EMPTY_ARRAY) && $value === []) ||
			(($s & self::INTEGER) && $value === 0) ||
			(($s & self::FLOAT) && $value === 0.0)  ||
			(($s & self::BOOLEAN) && $value === false) ||
			(($s & self::ZERO_STRING) && $value === '0')
		)
		{
			return null;
		}

		throw new IllegalCastException;
	}

	/**
	 * Check if a value is null.
	 *
	 * @param $value     The value to check
	 *
	 * @return boolean   true if $value === null
	 */
	public function validate($value)
	{
		return $value === null;
	}
}